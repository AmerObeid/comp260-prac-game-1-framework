﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public string horizontalAxis;
	public string verticalAxis;

	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 3.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 500.0f;
	public float turnSpeed = 30.0f;

	// Use this for initialization
	void Start () {
		
	}
		
	//public float maxSpeed = 5.0f;

	// Update is called once per frame
	void Update () {

		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxis);
		direction.y = Input.GetAxis(verticalAxis);

		//Vector2 velocity = direction * maxSpeed;

		//transform.Translate(velocity * Time.deltaTime);

		float turn = Input.GetAxis("Horizontal");

		// turn the car
		transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {
				speed = speed + brake * Time.deltaTime;
			}
		}
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
