﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{
	//public float speed = 4.0f;
	//public float turnSpeed = 180.0f;
	public Transform player1;
	public Transform player2;
	//private Transform target;
	//public Vector2 heading = Vector3.right;

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	// private state
	private float speed;
	private float turnSpeed;

	public Transform target;
	private Vector2 heading;

	// Use this for initialization
	void Start (){
		// find the player to set the target
		PlayerMove p = FindObjectOfType<PlayerMove>();
		target = p.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, 
			Random.value);
	}

	

	// Update is called once per frame
	void Update ()
	{
		/*
		Vector3 direction1 = player1.position - transform.position;
		Vector3 direction2 = player2.position - transform.position;

		if (direction1.magnitude < direction2.magnitude) {
			target = player1;
		} else {
			target = player2;
		}
		*/


		Vector2 direction = target.position - transform.position;

		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		} else {
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);

	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay (transform.position, direction);
	}

}