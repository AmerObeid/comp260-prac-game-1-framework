﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public PlayerMove player;
	public int nBees = 50;
	public float xMin, yMin;
	public float width, height;


	// Use this for initialization
	void Start () {
		

		//BeeMove bee = Instantiate(beePrefab);
		//bee.target = player.transform;

		//target = player.transform;

		for (int i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate (beePrefab);

			bee.transform.parent = transform;            
			bee.gameObject.name = "Bee " + i;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2(x,y);
		}

	}
	
	// Update is called once per frame
	void Update () {
		PlayerMove player = 
			(PlayerMove) FindObjectOfType(typeof(PlayerMove));
		//target = player.transform;
		
	}
}
